const win = nw.Window.get()
const tray = new nw.Tray({title: '邮件', icon: 'app/src/logo.png',tooltip:'邮件系统\n点击显示主界面'});
tray.on('click', () => {
    win.restore()
    win.focus()
})

var menu = new nw.Menu();
menu.append(new nw.MenuItem({
    type: 'normal',
    label: '显示主界面' ,
    click: () => {
        win.restore()
        win.focus()
    }}));
menu.append(new nw.MenuItem({
    type: 'normal',
    label: '退出系统',
    click: () => {
        win.close()
    }}));
tray.menu = menu;
const app = new Vue
(
    {
        el: '#app',
        data:
        {
            color:'',
            chose:1,
            account:JSON.parse(localStorage.getItem('account')),

            newid:'',

            password:'',
            newpassword:'',
            repassword:'',

            delpassword:'',
            confire:'',

            infomation:'',
            error: false

        },
        methods:
        {
            closewindow()
            {
                win.hide()
            },
            sentinfo(info)
            {
                app.infomation = info
                app.error = true
                setTimeout(function()
                {
                    app.error = false
                }, 2000)
            },
            cs1()
            {
                this.chose = 1
            },
            cs2()
            {
                this.chose = 2
            },
            cs3()
            {
                this.chose = 3
            },
            cs4()
            {
                this.chose = 4
                tray.remove()
                tray = null
                location.href='index.html'
            },
            async ChangeId()
            {
                const res = await axios
                ({
                    url:'http://mail.zhoujump.club:8080/get/changeid',
                    params:
                        {
                            account:this.account.account,
                            password:this.account.password,
                            newid:this.newid,
                        }
                })
                if(res.data === "Success")
                {
                    this.sentinfo('修改完成，请重新登录。')
                    location.href='login.html'
                }
                else
                {
                    this.sentinfo('账号验证失败，请重新登录')
                }
            },
            async ChangePasswd()
            {
                if(this.newpassword !== this.repassword)
                {
                    this.sentinfo("确认密码不符")
                    return
                }
                const res = await axios
                ({
                    url:'http://mail.zhoujump.club:8080/get/changepasswd',
                    params:
                        {
                            account:this.account.account,
                            password:this.password,
                            newpasswd:this.newpassword,
                        }
                })
                if(res.data === "Success")
                {
                    this.sentinfo('修改完成，请重新登录。')
                    location.href='login.html'
                }
                else
                {
                    this.sentinfo('账号验证失败，请重新登录')
                }
            },
            async getColor()
            {
                const res = await axios
                ({
                    url:'http://mail.zhoujump.club:8080/get/getcolor',
                    params:
                        {
                            account: this.account.account,
                            password: this.account.password,
                        }
                })
                this.color = res.data
            },
            async Delaccount()
            {
                if(this.confire !== "注销账户")
                {
                    this.sentinfo("请确认")
                    return
                }
                console.log(this.account.account)
                const res = await axios
                ({
                    url:'http://mail.zhoujump.club:8080/get/delaccount',
                    params:
                        {
                            account:this.account.account,
                            password:this.delpassword,
                        }
                })
                console.log(res.data)
                if(res.data === "Success")
                {
                    this.sentinfo('修改完成，请重新登录。')
                    location.href='signup.html'
                }
                else
                {
                    this.sentinfo('账号验证失败，请重新登录')
                }
            },
        },
        mounted()
        {
            this.getColor()
        }
    }
)