const win = nw.Window.get()
const tray = new nw.Tray({title: '邮件', icon: 'app/src/logo.png',tooltip:'邮件系统\n点击显示主界面'});
tray.on('click', () => {
    win.restore()
    win.focus()
})

var menu = new nw.Menu();
menu.append(new nw.MenuItem({
    type: 'normal',
    label: '显示主界面' ,
    click: () => {
        win.restore()
        win.focus()
    }}));
menu.append(new nw.MenuItem({
    type: 'normal',
    label: '新建邮件',
    click: () => {
        win.restore()
        win.focus()
        app.setwrite()
    }}));
menu.append(new nw.MenuItem({
    type: 'normal',
    label: '账户中心',
    click: () => {
        win.restore()
        win.focus()
        location.href='account.html'
        tray.remove()
    }}));
menu.append(new nw.MenuItem({
    type: 'normal',
    label: '退出系统',
    click: () => {
        win.close()
    }}));
tray.menu = menu;
const topbar = new Vue
(
    {
        el: '#topbar',
        data:
            {
                searchlist:[],
                keyword:'',
                mode:'none',
                color:'#479421',
                account:JSON.parse(localStorage.getItem('account')),
                error: false,
                infomation: '',
                start:true,
                ishide:false,
                sidebar:false,
                max:false,

            },
        methods:
            {
                colorapply()
                {
                    account.color = this.color
                    app.color = this.color
                    topbar.color = this.color
                    view.color = this.color
                    write.color = this.color
                },
                async setcolor()
                {
                    if (this.colorset === false)
                        return
                    const res = await axios
                    ({
                        url:'http://mail.zhoujump.club:8080/get/setcolor',
                        params:
                            {
                                account:this.account.account,
                                password:this.account.password,
                                color:this.color
                            }
                    })
                    
                },
                maxwindow()
                {
                    const win = nw.Window.get()
                    if(this.max === true)
                    {
                        win.restore()
                        this.max = false
                    }
                    else
                    {
                        win.maximize()
                        this.max = true
                    }
                },
                miniwindow()
                {
                    win.minimize()
                },
                closewindow()
                {
                    win.hide()
                },
                webvesion()
                {
                    const win = nw.Window.get();
                    nw.Shell.openExternal('http://mail.zhoujump.club/')
                    win.close(true)
                },
                logout()
                {
                    topbar.account.account = ''
                    topbar.account.password = ''
                    localStorage.setItem('account',JSON.stringify(topbar.account))
                    tray.remove()
                    location.href='login.html'
                },
                changesidebar()
                {
                    if(topbar.sidebar === true)
                    {
                        topbar.sidebar = false
                        this.setcolor()
                    }
                    else
                        topbar.sidebar = true
                },
                sentinfo(info)
                {
                    topbar.infomation = info
                    topbar.error = true
                   setTimeout(function()
                    {
                        topbar.error = false
                    }, 2000)
                },
                setview(id)
                {
                    view.mode = 'view'
                    write.mode = 'none'
                    account.mode  = 'none'
                    view.letter = app.msglist[id]
                    reader.setHtml(view.letter.text)
                    topbar.mode = 'none'
                },
                sear()
                {
                    let keyword = topbar.keyword
                    if(keyword === '')
                        topbar .mode= 'none'
                    else {
                        topbar.searchlist = app.msglist.filter(item => item.account.includes(keyword) === true || item.theme.includes(keyword) === true)
                        topbar.mode = 'view'
                    }
                },
                searhide()
                {
                    topbar.mode = 'none'
                }

            },
        watch:
            {
                keyword(keyword)
                {
                    if(keyword === '')
                        topbar.searchlist=[]
                    else {
                        topbar.searchlist = app.msglist.filter(item => item.account.includes(keyword) === true || item.theme.includes(keyword) === true)
                        topbar.mode = 'view'
                    }
                }
            }
    }
)
const app = new Vue
(
    {
        el: '#app',
        data:
            {
                friend:'',
                add:false,
                color:'#b6133f',
                acclist:
                    [
                        {friend: '张三',account:'zhoujump'},
                    ],
                msglist:JSON.parse(localStorage.getItem('msglist'+topbar.account.account.toString())) || [],
                box:true,
            },
        methods:
            {
                newmsg()
                {
                    this.$refs.new.play()
                },
                sentmsg()
                {
                    this.$refs.sent.play()
                },
                delmsg()
                {
                    this.$refs.del.play()
                },
                addbut()
                {
                    this.add = !this.add
                },
                chosebox()
                {
                    this.box = true
                },
                choseacc()
                {
                    this.box = false
                },
                setview(index)
                {
                    view.mode = 'view'
                    view.letter = app.msglist[index]
                    reader.setHtml(view.letter.text)
                    view.mode = 'view'
                    write.mode = 'none'
                    account.mode = 'none'
                },
                clicknoti()
                {
                    win.restore()
                    win.focus()
                    view.letter = app.msglist[0]
                    reader.setHtml(view.letter.text)
                    view.mode = 'view'
                    write.mode = 'none'
                    account.mode = 'none'
                },
                setwrite()
                {
                    write.mode = 'view'
                    view.mode = 'none'
                    account.mode = 'none'
                },
                setaccount(acc)
                {
                    write.mode = 'none'
                    view.mode = 'none'
                    account.acc = acc
                    account.mode = 'view'
                },
                async getfriend()
                {
                    const res = await axios
                    ({
                        url:'http://mail.zhoujump.club:8080/get/selfriend',
                        params:
                            {
                                account:topbar.account.account,
                            }
                    })
                    console.log(res.data)
                    this.acclist = res.data
                },
                async addfriend()
                {
                    if(this.friend === '')
                    {
                        topbar.sentinfo('你想添加啥？')
                        return
                    }
                    const res = await axios
                    ({
                        url:'http://mail.zhoujump.club:8080/get/insertfriend',
                        params:
                            {
                                account:topbar.account.account,
                                friend: this.friend
                            }
                    })
                    console.log(res.data)
                    if (res.data === "已经有了")
                    {
                        topbar.sentinfo('你添加过他了')
                        return
                    }
                        await this.getfriend()
                        topbar.sentinfo('添加成功')
                        account.acc=this.friend
                        account.mode="view"
                },
                async update()
                {
                    const res = await axios
                    ({
                        url:'http://mail.zhoujump.club:8080/get/getmail',
                        params:
                            {
                                account:topbar.account.account,
                                password:topbar.account.password
                            }
                    })
                    if(res.data.length === 0)
                    {
                    }
                    else
                    {
                        console.log(res.data)
                        app.msglist = res.data.concat(app.msglist)
                        topbar.sentinfo('收到新消息')
                        var n = new Notification("新邮件",{ body: "来自'"+res.data[0].account+"'等的新消息" });
                        n.onclick = app.clicknoti
                        n.onshow = function () {
                            setTimeout(n.close.bind(n), 5000);
                        }
                        app.newmsg()
                    }
                }
            },
        mounted()
        {
            this.getfriend()
            console.log(topbar.account.account)
            if(topbar.account.account === '') {
                tray.remove()
                location.href = 'login.html'
            }
            this.update
            if (this.timer) {
                clearInterval(this.timer);
            } else {
                this.timer = setInterval(this.update, 3000);
            }
        },
        beforeDestroy() {
            clearInterval(this.timer);
        },
        watch:
            {
                msglist:
                    {
                        deep: true,
                        handler(msg)
                        {
                            localStorage.setItem('msglist'+topbar.account.account.toString(),JSON.stringify(msg))
                        }
                    }
            }
    }
);
const view = new Vue
(
    {
        el: '#view',
        data:
            {
                color:'#479421',
                mode:'none',
                letter:{id:'1',type:'get',account:'张三',time:'2003/6/1 6:30:03',theme:'张三的信件',text:'text'}
            },
        methods:
            {
                addfriend()
                {
                    app.friend = this.letter.account
                    app.addfriend()
                },
                remail(account)
                {
                    write.letter.account = account
                    if(this.letter.theme.substring(0,3) === '回复：')
                        write.letter.theme = this.letter.theme
                    else
                        write.letter.theme = "回复：" + this.letter.theme
                    this.close_view()
                    app.setwrite()
                },
                close_view()
                {
                    this.mode = 'none'
                },
                del(id)
                {
                    console.log('del'+id)
                    app.msglist = app.msglist.filter(item => item.id !== id)
                    topbar.sentinfo('邮件已删除')
                    app.delmsg()
                    this.mode = 'none'
                }
            }
    }
);
const account = new Vue
(
    {
        el: '#account',
        data:
            {
                color:'#479421',
                mode:'none',
                acc:'张三'
            },
        methods:
            {
                async delfriend()
                {
                    const res = await axios
                    ({
                        url:'http://mail.zhoujump.club:8080/get/delfriend',
                        params:
                            {
                                account:topbar.account.account,
                                friend:this.acc
                            }
                    })
                    console.log(res.data)
                    app.delmsg()
                    this.mode="none"
                    await app.getfriend()
                    topbar.sentinfo('删除成功')
                },
                close_view()
                {
                    this.mode = 'none'
                },
                del(id)
                {
                    app.msglist = app.msglist.filter(item => item.id !== id)
                    topbar.sentinfo('邮件已删除')
                    app.delmsg()
                    this.mode = 'none'
                },
                setwrite()
                {
                    write.letter.account = this.acc
                    this.close_view()
                    app.setwrite()
                },
                search()
                {
                    topbar.keyword = this.acc
                    topbar.sear()
                },
            }
    }
);
const write = new Vue
(
    {
        el: '#write',
        data:
            {
                color:'#ffffff',
                mode:'none',
                letter:{account:'',theme:'',text:''}
            },
        methods:
            {
                close_write()
                {
                    this.mode = 'none'
                },
                async getColor()
                {
                    const res = await axios
                    ({
                        url:'http://mail.zhoujump.club:8080/get/getcolor',
                        params:
                            {
                                account: topbar.account.account,
                                password: topbar.account.password,
                            }
                    })
                    this.color = res.data
                    topbar.color = this.color
                    app.color = this.color
                    view.color = this.color
                    write.color = this.color
                    account.color = this.color
                },
                async sentmail()
                {
                    this.letter.text = editor.getHtml()
                    if(write.letter.theme === '' || write.letter.text ==='')
                    {
                        topbar.sentinfo('请输入主题与正文')
                        return
                    }
                    const _this = this;
                    let yy = new Date().getFullYear();
                    let mm = new Date().getMonth()+1;
                    let dd = new Date().getDate();
                    let hh = new Date().getHours();
                    let mf = new Date().getMinutes()<10 ? '0'+new Date().getMinutes() : new Date().getMinutes();
                    let ss = new Date().getSeconds()<10 ? '0'+new Date().getSeconds() : new Date().getSeconds();
                    _this.gettime = yy+'/'+mm+'/'+dd+' '+hh+':'+mf+':'+ss;
                    let newletter =
                        {
                            id:Math.floor(Math.random() * 100),
                            type:'sent',
                            account:write.letter.account,
                            time:_this.gettime,
                            theme:write.letter.theme,
                            text:write.letter.text,
                            from:""
                        }
                    const res = await axios
                    ({
                        url:'http://mail.zhoujump.club:8080/get/sentmail',
                        params:
                            {
                                account:topbar.account.account,
                                password:topbar.account.password,
                                text:write.letter.text,
                                theme:write.letter.theme,
                                from:write.letter.account
                            }
                    })
                    if(res.data === 'Success')
                    {
                        console.log(res.data)
                        topbar.sentinfo('发送成功')
                        app.msglist.unshift(newletter)
                        view.letter = app.msglist[0]
                        view.mode = 'view'
                        write.mode = 'none'
                        app.sentmsg()
                    }
                    else if (res.data === 'nouser')
                        topbar.sentinfo('收件人不存在')
                    else if (res.data === 'AccountError')
                    {
                        topbar.sentinfo('请重新登录')
                        location.href='login.html'
                    }
                    else if (write.letter.theme === '')
                        topbar.sentinfo('请输入主题')
                    else
                        topbar.sentinfo('服务器出错拉')

                }
            },
        mounted()
        {
            this.getColor()
            topbar.color = this.color
            app.color = this.color
            view.color = this.color
            write.color = this.color
        }
    }
)
