if(JSON.parse(localStorage.getItem('account')) === null)
    nw.Window.open('app/login.html', {"width": 1080, "height": 750, "position": "center", "min_width": 1080, "min_height": 750, "icon": "app/src/icon_128.png" ,"frame": false});
else
    nw.Window.open('app/index.html', {"width": 1080, "height": 750, "position": "center", "min_width": 1080, "min_height": 750, "icon": "app/src/icon_128.png" ,"frame": false});
