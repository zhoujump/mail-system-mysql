package com.easymail.sever.controller;

import com.easymail.sever.entity.Contact;
import com.easymail.sever.entity.Mail;
import com.easymail.sever.entity.Setting;
import com.easymail.sever.entity.User;
import com.easymail.sever.mapper.ContactMapper;
import com.easymail.sever.mapper.MailMapper;
import com.easymail.sever.mapper.SettingMapper;
import com.easymail.sever.mapper.UserMapper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@RestController
@CrossOrigin
@RequestMapping(value = "/get")
public class LoginController
{
    @Resource
    UserMapper userMapper;
    @Resource
    MailMapper mailMapper;
    @Resource
    SettingMapper settingMapper;
    @Resource
    ContactMapper contactMapper;

    @GetMapping("/insertfriend")
    public  String Insertfriend(String account,String friend)
    {
        System.out.println("insert");
        Contact contact = contactMapper.findByAccount(account,friend);
        if (contact == null)
        {
            contactMapper.AddByName(account,friend);
            return "Success";
        }
        else
            return "已经有了";
    }

    @GetMapping("/delfriend")
    public String Delfriend(String account,String friend)
    {
        System.out.println("delete");
        contactMapper.DelByName(account,friend);
        return "Success";
    }

    @GetMapping("/selfriend")
    public List<Contact> Selfrienf(String account)
    {
        System.out.println("select");
        List<Contact> contact = contactMapper.findByName(account);
        return contact;
    }

    @GetMapping("/getcolor")
    public String Getcolor(String account,String password)
    {
        System.out.println("get");
        User User = userMapper.findByAccount(account);
        if(User == null)
            return "NoAccount";
        else if (!Objects.equals(User.getPassword(), password))
            return "WrongPassword";
        else {
            Setting Setting = settingMapper.findColorByAccount(account);
            return Setting.getColor();
        }
    }
    @GetMapping("/setcolor")
    public String Getcolor(String account,String password,String color)
    {
        User User = userMapper.findByAccount(account);
        if(User == null)
            return "NoAccount";
        else if (!Objects.equals(User.getPassword(), password))
            return "WrongPassword";
        else {
            settingMapper.Changecolor(account,color);
            return "Success";
        }
    }
    @GetMapping("/changeid")
    public String ChangeId(String account,String password,String newid)
    {
        User User = userMapper.findByAccount(account);
        if(User == null)
            return "NoAccount";
        else if (!Objects.equals(User.getPassword(), password))
            return "WrongPassword";
        else {
            userMapper.Changeid(newid,account);
            System.out.println("ok");
            return "Success";
            }
    }
    @GetMapping("/changepasswd")
    public String ChangePasswd(String account,String password,String newpasswd)
    {
        String genshin = "启动";
        User User = userMapper.findByAccount(account);
        if(User == null)
            return "NoAccount";
        else if (!Objects.equals(User.getPassword(), password))
            return "WrongPassword";
        else {
            userMapper.Changepasswd(account,newpasswd);
            return "Success";
        }
    }
    @GetMapping("/delaccount")
    public String DelAccount(String account,String password)
    {
        User User = userMapper.findByAccount(account);
        if(User == null)
            return "NoAccount";
        else if (!Objects.equals(User.getPassword(), password))
            return "WrongPassword";
        else {
            userMapper.Delaccount(account,password);
            return "Success";
        }
    }
    @GetMapping("/login")
    public String Login(String account,String password)
    {
        User User = userMapper.findByAccount(account);
        if(User == null)
            return "NoAccount";
        else if (!Objects.equals(User.getPassword(), password))
            return "WrongPassword";
        else
            return "Success";
    }

    @GetMapping("/signup")
    public String Signup(String account,String password)
    {
        User User = userMapper.findByAccount(account);
        if(User == null)
        {
            settingMapper.insertColor(account,"#3877ec");
            userMapper.SignUp(account, password);
            mailMapper.InsertMail
                    (
                            "系统",
                                "<h1 style=\"text-align: center;color: #3877ec\">欢迎使用</h1><h3 style=\"text-align: center;color: #3877ec\">你可以点击‘发邮件’按钮来发送第一份邮件了</h3><h2 style=\"text-align: center;color: #c9c9c9\">关注项目:<a href=\"https://gitee.com/zhoujump/mail-system-mysql\" target=\"_blank\" style=\"color: #c9c9c9\">码云</a><a href=\"https://github.com/ZhouJump/mail-system-mysql\" target=\"_blank\" style=\"color: #c9c9c9\">github</a></h2>",
                            "欢迎使用",
                                    account
                            );
            return "Success";
        }
        else
            return "HaveAccount";
    }

    @GetMapping("/sentmail")
    public String SentMail(String account,String password,String text,String theme,String from)
    {
        if (!Objects.equals(Login(account, password), "Success"))
        {
            System.out.println("InsertMail:FAIL");
            return "AccountError";
        } else if (userMapper.findByAccount(from) == null)
        {
            return "nouser";
        } else
        {
            mailMapper.InsertMail(account, text, theme, from);
            System.out.println("InsertMail:OK");
            return "Success";
        }
    }

    @GetMapping("/getmail")
    public List<Mail> Getmail(String account, String password)
    {
        if (!Objects.equals(Login(account, password), "Success"))
        {
            System.out.println("InsertMail:FAIL");
            return null;
        }
        else
        {
            List<Mail> Mail = mailMapper.findMailByAccount(account);
            mailMapper.delMailByAccount(account);
            return Mail;
        }

    }

    @GetMapping("/test")
    public String Test()
    {
        return "Success";
    }
}
