package com.easymail.sever.entity;

import lombok.Data;

@Data
public class Contact
{
    private String account;
    private String friend;
}
