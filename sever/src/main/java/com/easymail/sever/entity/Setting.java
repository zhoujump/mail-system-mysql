package com.easymail.sever.entity;

import lombok.Data;

@Data
public class Setting
{
    private String account;
    private String color;
}
