package com.easymail.sever.mapper;

import com.easymail.sever.entity.Contact;
import com.easymail.sever.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface ContactMapper
{
    @Update("insert into contact (account,friend) VALUES ('${account}','${friend}')")
    void AddByName(@Param("account") String account,@Param("friend") String friend);

    @Delete("delete from contact where friend = #{friend} and account = #{account}")
    void DelByName(@Param("account") String account,@Param("friend") String friend);

    @Select("select * from contact where account = #{account}")
    List<Contact> findByName(String account);

    @Select("select * from contact where account = #{account} and friend = #{friend}")
    Contact findByAccount(@Param("account") String account,@Param("friend") String friend);

}
