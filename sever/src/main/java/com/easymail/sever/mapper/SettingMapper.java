package com.easymail.sever.mapper;

import com.easymail.sever.entity.Mail;
import com.easymail.sever.entity.Setting;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface SettingMapper
{
    @Select("select * from setting where account = '${account}'")
    Setting findColorByAccount(String account);

    @Update("insert into setting (account,color) VALUES ('${account}','${color}')")
    void insertColor(@Param("account") String account,@Param("color") String color);

    @Update("update setting set color = #{color} where account = '#{account}'")
    void Changecolor(@Param("account") String account,@Param("color") String color);

}
