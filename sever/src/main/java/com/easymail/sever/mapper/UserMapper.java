package com.easymail.sever.mapper;

import com.easymail.sever.entity.User;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

public interface UserMapper
{
    @Select("select * from user")
    List<User> findAll();

    @Select("select * from user where account = #{account}")
    User findByAccount(String account);

    @Update("insert into user (account,password) VALUES ('${account}','${password}')")
    void SignUp(@Param("account") String account, @Param("password") String password);

    @Update("update user set account = #{newid} where account = #{account}")
    void Changeid(@Param("newid") String newid, @Param("account") String account);

    @Update("update  user set password = #{newpasswd} where account = #{account}")
    void Changepasswd(@Param("account") String account,@Param("newpasswd") String newpasswd);
    @Delete("delete from user where account = #{account}")
    void Delaccount(@Param("account") String account,@Param("password") String password);
}
